#!/bin/bash

echo 'usage ex.:

./search.sh jhonas 12345
./search.sh '' 12345

first parameter is nome and the second is cpf

'

token=$(cat token)

curl -0 -v -X GET http://localhost:3001/api/cadastro/consulta\?nome=$1\&cpf=$2 \
	-H "Expect:" \
	-H "Content-Type: application/json; charset=utf-8"
	-H "x-access-token: $token"

