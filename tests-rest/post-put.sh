#!/bin/bash

echo 'usage ex.:

./post-put.sh post cadastro/ok.json
./post-put.sh put cadastro/with-id/ok2.json

'

token=$(cat token)

curl -0 -v -X ${1^^} http://localhost:3001/api/cadastro \
	-H "Expect:" \
	-H "Content-Type: application/json; charset=utf-8" \
	-H "x-access-token: $token" \
	-d @$2

