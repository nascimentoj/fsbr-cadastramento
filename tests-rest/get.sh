#!/bin/bash

echo 'usage ex.: 

get list of cadastro:
./get.sh

search by cpf:
./get.sh $cpf

'

token=$(cat token)

curl -0 -v http://localhost:3001/api/cadastro/$1 \
	-H "Expect:" \
	-H "Content-Type: application/json; charset=utf-8" \
	-H "x-access-token: $token"

