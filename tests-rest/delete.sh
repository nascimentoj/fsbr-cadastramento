#!/bin/bash

echo 'usage ex.: 

./delete.sh 43

just inform id to delete
'

token=$(cat token)

curl -0 -v -X DELETE http://localhost:3001/api/cadastro/$1 \
	-H "Expect:" \
	-H "Content-Type: application/json; charset=utf-8"
	-H "x-access-token: $token"

