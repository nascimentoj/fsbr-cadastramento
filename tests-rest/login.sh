#!/bin/bash

echo 'usage ex.:

./login.sh email@provider.com password

'

node jsonkey.js $(curl -X POST http://localhost:3001/api/login \
	-H "Expect:" \
	-H "Content-Type: application/json; charset=utf-8" \
	-d "{ \"email\": \"$1\", \"senha\": \"$2\" }"
) token > token

