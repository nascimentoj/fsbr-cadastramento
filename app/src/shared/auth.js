import { createContext, useContext, useState } from 'react'
import { useHistory, Route, Redirect } from 'react-router-dom'
import decodeToken from 'jwt-decode'
import api from './auth-api'

const authContext = createContext()

export function useAuth () {
  return useContext(authContext)
}

function useProvideAuth() {
  const [token, setToken] = useState(localStorage.token)

  async function login(credentials) {
    const res = await api.login(credentials)
    if (res.token) {
      const { token } = res
      localStorage.token = token
      setToken(token)
    }

    return res
  }

  function logout() {
    delete localStorage.token
    setToken(null)
  }

  function getUser() {
    if (!token) return {}
    const { id, nome, email } = decodeToken(token)
    return { id, nome, email }
  }

  return {
    token, login, logout, getUser
  }
}

export function AuthProvider ({ children }) {
  const auth = useProvideAuth()

  return (
    <authContext.Provider value={auth}>
      {children}
    </authContext.Provider>
  )
}

export function PrivateRoute ({ component: Component, ...rest }) {
  let auth = useAuth()

  function renderRoute ({ location, ...props }) {
    return auth.token
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: location } }} />
  }

  return (
    <Route {...rest} render={renderRoute} />
  )
}

export function PublicRoute ({ component: Component, ...rest }) {
  let auth = useAuth()

  function renderRoute ({ location, ...props }) {
    return auth.token && location.pathname === '/login'
      ? <Redirect to={{ pathname: '/', state: { from: location } }} />
      : <Component {...props} />
  }

  return (
    <Route {...rest} render={renderRoute} />
  )
}

export function useAuthRedirect () {
  const history = useHistory()
  const auth = useAuth()

  return (res) => {
    if (res.status === 401) {
      auth.logout()
      history.push('/login')
      return true
    } else if (res.status === 500) {
      // TODO: implement danger toast
      alert(res.error.message)
      return true
    }
    return false
  }
}

