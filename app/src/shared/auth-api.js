import { post } from 'shared/api'

const api = {
  login: async credentials => post('/login', credentials)
}

export default api

