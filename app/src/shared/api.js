const URL = process.env.REACT_APP_ENVIRONMENT === 'dev'
  ? 'http://localhost:3001/api'
  : 'https://set-here-prod.url/api'

export const getUrl = sufix => `${URL}${sufix}`

const toParams = data => Object.keys(data).map(key =>
    encodeURIComponent(key) + '=' + encodeURIComponent(data[key])
).join('&')

const getUrlParams = (url, data) => `${getUrl(url)}?${toParams(data)}`

const fetchOptions = {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
  },
  mode: 'cors',
  cache: 'default'
}

const withToken = options => localStorage.token ? ({
  ...options, headers: {
    ...options.headers, 'x-access-token': localStorage.token
  }
}) : options

const getJson = async req => {
  const res = await req
  const json = await res.json()
  json.status = res.status

  // another way to force logout
  // if (res.status === 401) delete localStorage.token

  return json
}

export const get = async (url, data, token) => getJson(
  fetch(
    // define url, with or without params
    data ? getUrlParams(url, data) : getUrl(url),
    // define option like headers and authentication token
    withToken(fetchOptions)
  )
)

export const post = async (url, data, token) => getJson(fetch(getUrl(url), withToken({
  ...fetchOptions, method: 'POST', body: JSON.stringify(data)
})))

export const put = async (url, data, token) => getJson(fetch(getUrl(url), withToken({
  ...fetchOptions, method: 'PUT', body: JSON.stringify(data)
})))

export const del = async (url, data, token) => getJson(fetch(getUrl(url), withToken({
  ...fetchOptions, method: 'DELETE', body: JSON.stringify(data)
})))

