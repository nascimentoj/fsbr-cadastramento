import React from 'react'
import { useLocation, Switch, Redirect } from 'react-router-dom'
import { PublicRoute, PrivateRoute } from 'shared/auth'
import Login  from './features/login'
import Main  from './features/main'
import List from './features/cadastro/list'
import Find from './features/cadastro/find'
import Add from './features/cadastro/add'
import Edit from './features/cadastro/edit'
import Remove from './features/cadastro/remove'

export const routes = ({
  login: { path: '/login', title: 'Login' },
  main: { path: '/principal', title: 'Cadastramento' },
  list: { path: '/listagem', title: 'Listagem' },
  find: { path: '/consulta', title: 'Consulta' },
  add: { path: '/inclusao', title: 'Inclusão' },
  edit: { path: '/alteracao', title: 'Alteração' },
  remove: { path: '/exclusao', title: 'Exclusão' },
})

const Routes = () => (
  <Switch>
    <Redirect exact from="/" to={routes.main.path} />
    <PublicRoute path={routes.login.path} component={Login} />
    <PrivateRoute path={routes.main.path} component={Main} />
    <PrivateRoute path={routes.list.path} component={List} />
    <PrivateRoute path={routes.find.path} component={Find} />
    <PrivateRoute path={routes.add.path} component={Add} />
    <PrivateRoute path={routes.edit.path} component={Edit} />
    <PrivateRoute path={routes.remove.path} component={Remove} />
    <Redirect to={routes.main.path} />
  </Switch>
)

export const useCurrent = () => {
  const location = useLocation()
  return Object.values(routes).find(r => r.path === location.pathname)
}

export default Routes

