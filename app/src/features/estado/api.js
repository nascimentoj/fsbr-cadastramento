import { get } from 'shared/api'

const api = {
  get: () => get('/estado')
}

export default api

