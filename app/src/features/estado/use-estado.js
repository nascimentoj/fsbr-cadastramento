import { useState, useEffect } from 'react'
import { useAuthRedirect } from 'shared/auth'
import api from './api'

export default function useEstados () {
  const [estados, setEstados] = useState([])
  const authRedirect = useAuthRedirect()

  useEffect(() => {
    const fetchEstados = async () => {
      const res = await api.get()
      const redirected = authRedirect(res)
      if (!redirected) {
        const emptyEstado = { id: '', estado: 'Selecione...' }
        setEstados([ emptyEstado, ...res ])  
      }
    }

    if (!estados.length) fetchEstados()
  }, [authRedirect, estados.length])

  return estados
}
