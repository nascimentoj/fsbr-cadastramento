import { useCurrent } from 'routes'

export default function Menu({ children }) {
  const currentRoute = useCurrent()

  return (
    <section className="App container mt-5">
      <div className="card">
        <header className="card-header">
          <div className="row">
            <div className="col-1">
              <img
                src={`${process.env.PUBLIC_URL}/logo192.png`}
                width="25"
                alt="FSBR Logo"
                style={{ marginRight: '2px' }}
              />
              <span className="float-end">FSBR</span>
            </div>
            <div className="col">
              <h5 className="card-title text-center">{currentRoute && currentRoute.title}</h5>
            </div>
          </div>
        </header>
        <main className="card-body">
          {children}
        </main>
      </div>
    </section>
  )
}

