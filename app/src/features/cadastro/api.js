import { get, post, put, del } from 'shared/api'

const url = '/cadastro'

const api = {
  list: () => get(url),
  search: (filter) => get(`${url}/consulta`, filter),
  findByCpf: cpf => get(`${url}/${cpf}`),
  add: cadastro => post(url, cadastro),
  edit: cadastro => put(url, cadastro),
  remove: id => del(`${url}/${id}`)
}

export default api

