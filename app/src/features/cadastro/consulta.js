import { useState } from 'react'
import { useAuthRedirect } from 'shared/auth'
import api from './api'

export default function Consulta({ onFound, onReset }) {
  const [cpf, setCpf] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  const authRedirect = useAuthRedirect()

  const resetCpf = () => {
    setCpf('')
    const $cpf = document.querySelector('#find-cpf')
    $cpf.focus()
    if (onReset) onReset()
  }

  async function submit (e) {
    e.preventDefault()
    setErrorMessage('')

    if(!/^\d{11}$/.test(cpf)) {
      setErrorMessage('CPF deve ser composto de 11 números')
      resetCpf()
    } else {
      const res = await api.findByCpf(cpf)
      authRedirect(res)
      if (res.error) {
        setErrorMessage(res.error.message)
        resetCpf()
      } else onFound(res)
    }
  }

  return (
    <div className="container">
      <form onSubmit={submit}>
        <div className="row mb-3">
          <label className="col-label-form col-2" htmlFor="find-cpf">CPF:</label>
          <div className="col-8">
            <input
              id="find-cpf"
              className={`form-control${errorMessage ? ' is-invalid' : ''}`}
              type="text"
              name="find-cpf"
              onChange={e => setCpf(e.target.value)}
              value={cpf}
            />
            <div id="nome-error" className="invalid-feedback">{errorMessage}</div>
          </div>
          <div className="col-2">
              <button className="btn btn-primary float-end">Buscar</button>
          </div>
        </div>
      </form>
      <hr />
    </div>
  )
}

