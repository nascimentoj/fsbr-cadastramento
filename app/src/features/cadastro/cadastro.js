import { useState, useEffect } from 'react'
import cls from 'classnames'

export default function Cadastro({ disabled, estados, cadastro, error, onSubmit, children }) {
  const [id, setId] = useState('')
  const [nome, setNome] = useState('')
  const [cpf, setCpf] = useState('')
  const [estado, setEstado] = useState('')
  const [cidade, setCidade] = useState('')

  useEffect(() => {
    if (cadastro && cadastro.id) {
      setId(cadastro.id)
      setNome(cadastro.nome)
      setCpf(cadastro.cpf)
      setEstado(cadastro.estado)
      setCidade(cadastro.cidade)
    } else if (id) {
      setId('')
      setNome('')
      setCpf('')
      setEstado('')
      setCidade('')
    }
  }, [cadastro, id])

  const disabledAttr = { disabled }

  function submit(e) {
    e.preventDefault()
    const cadastro = {
      id, nome, cpf, estado, cidade
    }
    if (onSubmit) onSubmit(cadastro)
  }

  const errors = ['nome', 'cpf', 'estado', 'cidade'].reduce((obj, field) => ({
    ...obj, [field]: error && error.field === field && error.message
  }), {})


  return (
    <form className="container" onSubmit={submit}>
      <div className="row mb-3">
        <label className="col-2 col-form-label" htmlFor="nome">Nome:</label>
        <div className="col-10">
          <input
            id="nome"
            name="nome"
            className={cls('form-control', { 'is-invalid': errors.nome })}
            type="text"
            {...disabledAttr}
            onChange={e => setNome(e.target.value)}
            value={nome}
            aria-describedby="nome-error"
          />
          <div id="nome-error" className="invalid-feedback">{errors.nome}</div>
        </div>
      </div>
      <div className="row mb-3">
        <label className="col-2 col-form-label" htmlFor="cpf">CPF:</label>
        <div className="col-10">
          <input
            id="cpf"
            name="cpf"
            className={cls('form-control', { 'is-invalid': errors.cpf })}
            type="text"
            {...disabledAttr}
            onChange={e => setCpf(e.target.value)}
            value={cpf}
            aria-describedby="cpf-error"
          />
          <div id="cpf-error" className="invalid-feedback">{errors.cpf}</div>
        </div>
      </div>
      <div className="row mb-3">
        <label className="col-2 col-form-label" htmlFor="estado">Estado:</label>
        <div className="col-10">
          <select
            id="estado"
            name="estado"
            className={cls('form-select', { 'is-invalid': errors.estado })}
            {...disabledAttr}
            onChange={e => setEstado(e.target.value)}
            value={estado}
            aria-describedby="estado-error"
          >
            {estados.map(e => <option key={e.id} value={e.id}>{e.estado}</option>)}
          </select>
          <div id="estado-error" className="invalid-feedback">{errors.estado}</div>
        </div>
      </div>
      <div className="row mb-3">
        <label className="col-2 col-form-label" htmlFor="cidade">Cidade:</label>
        <div className="col-10">
          <input
            id="cidade"
            name="cidade"
            className={cls('form-control', { 'is-invalid': errors.cidade })}
            type="text"
            {...disabledAttr}
            onChange={e => setCidade(e.target.value)}
            value={cidade}
            aria-describedby="cidade-error"
          />
          <div id="cidade-error" className="invalid-feedback">{errors.cidade}</div>
        </div>
      </div>
      <br />
      {children}
    </form>
  )
}
