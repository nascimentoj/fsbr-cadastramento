import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useAuthRedirect } from 'shared/auth'
import useEstado from '../estado/use-estado'
import Cadastro from './cadastro'
import Consulta from './consulta'
import GoBack from 'components/go-back'
import api from './api'

export default function Edit() {
  const [disabled, setDisabled] = useState(true)
  const [cadastro, setCadastro] = useState({})
  const [error, setError] = useState({})
  const history = useHistory()
  const authRedirect = useAuthRedirect()
  const estados = useEstado()
  
  const reset = () => { 
    setCadastro({})
    setDisabled(true)
  }

  function found(res) {
    if (res.id) {
      setDisabled(false)
      setCadastro(res)
    } else reset()
  }

  async function submit (cadastro) {
    const res = await api.edit(cadastro)
    const isRedirected = authRedirect(res)
    if (!isRedirected) {
      if (res.status === 200) {
        // TODO: implement success toast
        alert(res.message)
        history.goBack()
      } else setError(res.error)
    }
  }

  return (
    <>
      <Consulta onFound={found} onReset={reset} />
      <Cadastro estados={estados} cadastro={cadastro} error={error} onSubmit={submit} disabled={disabled}>
        <div className="text-center">
          <GoBack />
          <button className="btn btn-success ms-3" disabled={!cadastro.id}>Salvar</button>
        </div>
      </Cadastro>
    </>
  )
}

