import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import useEstado from '../estado/use-estado'
import { useAuthRedirect } from 'shared/auth'
import Cadastro from './cadastro'
import GoBack from 'components/go-back'
import api from './api'

export default function Add() {
  const [error, setError] = useState({})
  const history = useHistory()
  const authRedirect = useAuthRedirect()
  const estados = useEstado()
  
  async function submit (cadastro) {
    const res = await api.add(cadastro)
    const isRedirected = authRedirect(res)
    if (!isRedirected) {
      if (res.status === 200) {
        // TODO: implement success toast
        alert(res.message)
        history.goBack()
      } else setError(res.error)
    }
  }

  return (
    <Cadastro estados={estados} onSubmit={submit} error={error}>
      <div className="text-center">
        <GoBack />
        <button className="btn btn-success ms-3">Salvar</button>
      </div>
    </Cadastro>
  )
}

