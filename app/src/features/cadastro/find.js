import { useState } from 'react'
import useEstado from '../estado/use-estado'
import Cadastro from './cadastro'
import Consulta from './consulta'
import GoBack from 'components/go-back'

export default function Find() {
  const [cadastro, setCadastro] = useState({})
  const estados = useEstado()

  const reset = () => setCadastro({})

  function found(res) {
    if (res.id) setCadastro(res)
    else reset()
  }

  return (
    <>
      <Consulta onFound={found} onReset={reset} />
      <Cadastro estados={estados} cadastro={cadastro} disabled>
        <div className="text-center">
          <GoBack />
        </div>
      </Cadastro>
    </>
  )
}

