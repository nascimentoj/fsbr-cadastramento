import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useAuthRedirect } from 'shared/auth'
import useEstado from '../estado/use-estado'
import Cadastro from './cadastro'
import Consulta from './consulta'
import GoBack from 'components/go-back'
import api from './api'

export default function Remove() {
  const [cadastro, setCadastro] = useState({})
  const estados = useEstado()
  const history = useHistory()
  const authRedirect = useAuthRedirect()

  const reset = () => setCadastro({})

  function found(res) {
    if (res.id) setCadastro(res)
    else reset()
  }

  async function submit(cadastro) {
    const isRemoveCadastro = window.confirm('Deseja realmente excluir o registro?')
    if (!isRemoveCadastro) return

    const res = await api.remove(cadastro.id)
    const isRedirected = authRedirect(res)
    if (!isRedirected) {
      if (res.status === 200) {
        // TODO: implement success toast
        alert(res.message)
        history.goBack()
      } else alert(res.error.message)
    }
  }

  return (
    <>
      <Consulta onFound={found} onReset={reset} />
      <Cadastro estados={estados} cadastro={cadastro} onSubmit={submit} disabled>
        <div className="text-center">
          <GoBack />
          <button className="btn btn-warning ms-3" disabled={!cadastro.id}>Excluir</button>
        </div>
      </Cadastro>
    </>
  )
}

