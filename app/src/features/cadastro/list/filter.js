import { useState, useEffect } from 'react'
import api from '../api'

export default function Find({ clearSignal, onResult }) {
  const [nome, setNome] = useState('')
  const [cpf, setCpf] = useState('')

  useEffect(() => {
    setNome('')
    setCpf('')
    const $nome = document.querySelector('#find-nome')
    $nome.focus()
  }, [clearSignal])

  async function submit (e) {
    e.preventDefault()
    const promise = !nome && !cpf
      ? api.list()
      : api.search({ nome, cpf })
    onResult(await promise)
  }

  return (
    <div className="container">
      <form onSubmit={submit}>
        <div className="row mb-3">
          <label className="col-label-form col-1" htmlFor="find-cpf">Nome:</label>
          <div className="col-4">
            <input
              id="find-nome"
              className="form-control"
              type="text"
              name="find-nome"
              onChange={e => setNome(e.target.value)}
              value={nome}
            />
          </div>

          <label className="col-label-form col-1" htmlFor="find-cpf">CPF:</label>
          <div className="col-4">
            <input
              id="find-cpf"
              className="form-control"
              type="text"
              name="find-cpf"
              onChange={e => setCpf(e.target.value)}
              value={cpf}
            />
          </div>
          <div className="col-2">
              <button className="btn btn-primary float-end">Buscar</button>
          </div>
        </div>
      </form>
      <hr />
    </div>
  )
}

