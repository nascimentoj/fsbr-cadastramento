import { useState, useEffect } from 'react'
import { useAuthRedirect } from 'shared/auth'
import GoBack from 'components/go-back'
import api from '../api'
import Filter from './filter'

export default function List() {
  const [cadastros, setCadastros] = useState([])
  const [message, setMessage] = useState('')
  const [clearFilterSignal, setClearFilterSignal] = useState(0)
  const authRedirect = useAuthRedirect()

  function setResult (res) {
    const isRedirected = authRedirect(res)
    if (!isRedirected) {
      if (res.error) {
        setCadastros([])
        setMessage(res.error.message)
      } else {
        setCadastros(res)
        setMessage('')
      }
    }
    
  }

  useEffect(() => {
    const fetch = async () => {
      const res = await api.list()
      if (res.status === 200) {
        setCadastros(res)
      } else setMessage(res)
    }

    fetch()
  }, [])

  if (message.status) {
    authRedirect(message)
    return <></>
  }

  function clear () {
    setCadastros([])
    setClearFilterSignal(clearFilterSignal + 1)
  }

  return (
    <>
      <Filter clearSignal={clearFilterSignal} onResult={setResult} />
      <table className="table table-bordered border-primary">
        <thead className="table-primary">
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Nome</th>
            <th scope="col">CPF</th>
            <th scope="col">Cidade</th>
          </tr>
        </thead>
        <tbody>
          {cadastros.map(c =>
            <tr key={c.id}>
              <td>{c.id}</td>
              <td>{c.nome}</td>
              <td>{c.cpf}</td>
              <td>{c.cidade}</td>
            </tr>
          )}
          {message &&
            <tr>
              <td span="4" className="text-danger">{message}</td>
            </tr>
          }
        </tbody>
      </table>
      <span className="text-primary">Qtd.: {cadastros.length}</span>
      <br />
      <div className="text-center">
        <GoBack />
        <button type="button" className="btn btn-primary ms-3" onClick={clear}>Limpar</button>
      </div>
    </>
  )
}

