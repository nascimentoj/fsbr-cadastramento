import { useHistory, Link } from 'react-router-dom'
import { routes } from 'routes'
import { useAuth } from 'shared/auth'

export default function Main() {
  const auth = useAuth()
  const history = useHistory()
  const user = auth.getUser()

  function logout () {
    auth.logout()
    history.replace('/login')
  }

  return (
    <div className="main text-center">
      <p className="card-title text-center">{user.nome} &lt;{user.email}&gt;</p>
      <br />
      <br />
      <div className="row">
        <div className="col">
          <Link className="btn btn-primary" to={routes.add.path}>Incluir</Link>
        </div>
        <div className="col">
          <Link className="btn btn-primary" to={routes.edit.path}>Alterar</Link>
        </div>
        <div className="col">
          <Link className="btn btn-primary" to={routes.remove.path}>Excluir</Link>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col">
          <Link className="btn btn-primary" to={routes.find.path}>Consultar</Link>
        </div>
        <div className="col">
          <Link className="btn btn-primary" to={routes.list.path}>Listar</Link>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col">
          <button className="btn btn-danger" onClick={logout}>Sair</button>
        </div>
      </div>
    </div>
  )
}

