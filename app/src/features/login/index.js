import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useAuth } from 'shared/auth'
import cls from 'classnames'

export default function Login() {
  const [email, setEmail] = useState('')
  const [senha, setSenha] = useState('')
  const [error, setError] = useState({})
  const history = useHistory()
  const auth = useAuth()

  async function login(e) {
    e.preventDefault()
    const res = await auth.login({ email, senha })
    if (res.status === 200) {
      setError({})
      history.replace('/')
    } else {
      setError(res.error)
    }
  }

  const emailError = error.field === 'email'
  const senhaError = error.field === 'senha'

  return (
  <form className="container" onSubmit={login}>
    <div className="row mb-3">
      <label className="col-1 offset-4 col-form-label" htmlFor="email">E-mail:</label>
      <div className="col-3">
        <input
          id="email"
          name="email"
          className={cls('form-control', { 'is-invalid': emailError })}
          type="text"
          onChange={e => setEmail(e.target.value)}
          aria-describedby="email-error"
        />
        <div id="email-error" className="invalid-feedback">
          {emailError && error.message}
        </div>
      </div>
    </div>
    <div className="row mb-3">
      <label className="col-1 offset-4 col-form-label" htmlFor="senha">Senha:</label>
      <div className="col-3">
        <input
          id="senha"
          name="senha"
          className={cls('form-control', { 'is-invalid': senhaError })}
          type="password"
          onChange={e => setSenha(e.target.value)}
          aria-describedby="senha-error"
        />
        <div id="senha-error" className="invalid-feedback">
          {senhaError && error.message}
        </div>
      </div>
    </div>
    <div className={cls('row', 'mb-3', { 'd-none': !error.message || emailError || senhaError })}>
      <div id="login-error" className="offset-4 text-danger">{error.message}</div>
    </div>
    <div className="col-4 offset-4">
      <button className="btn btn-primary float-end">Login</button>
    </div>
  </form>
  )
}

