import React from 'react'
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.css'
import './index.css'
import { BrowserRouter as Router } from 'react-router-dom'
import { AuthProvider } from './shared/auth'
import Routes from './routes'
import Menu from './features/menu'

ReactDOM.render(
  <AuthProvider>
    <Router basename={`${process.env.PUBLIC_URL}/`}>
      <Menu>
        <Routes />
      </Menu>
    </Router>
  </AuthProvider>,
  document.getElementById('root')
)

