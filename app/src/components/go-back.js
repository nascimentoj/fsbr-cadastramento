import { useHistory } from 'react-router-dom'

export default function GoBack() {
  const history = useHistory()
  return <button
    type="button"
    className="btn btn-danger"
    onClick={() => history.goBack()}>
      Voltar
    </button>
}

