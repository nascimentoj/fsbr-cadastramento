# FSBR technical test

This is a SPA application created in response FSBR technical test.

The application rules are described in:
instructions.pdf (portuguese) (database access removed from document).

## About technologies and patterns:
The backend is using the technologies: nodejs, express and mysql adapter; with Clean architecture by Uncle Bob.
In the frontend i'm using react and bootstrap, it was generated with create-react-app.

![Uncle Bob's clean architecture representation](clean-architecture.jpg)

## Configuration
Rename the env file to .env and replace the values of variables with real ones

## Finished[ish]!
There are some details to adjust as you can see below in the todos
but for a test to show my skills I think it's good enough

### REST API:
 - [x] basic routing
 - [x] validation on backend
 - [x] proper error response into validation
 - [x] database CRUD
 - [x] authentication
 - [x] clean architecture
 - [x] CLI to manage usuario and not expose it in controllers (for security)
 - [ ] fix database freezing

### React Web client:
 - [x] CRA template
 - [x] Bootstrap working
 - [x] Base structure
 - [x] Authentication logic, login and redirections
 - [x] REST Consuming
 - [x] Fully funcional mock screens
 - [x] Screens:
	 - [x] Login
	 - [x] Main
	 - [x] List
	 - [x] Search one by cpf
	 - [x] Create
	 - [x] Update
	 - [x] Delete
	 - [ ] Implement modals and toasts instead alert and confirm
