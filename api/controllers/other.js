import { error } from '../helpers/http-response'

export default function makeOtherController() {
	return {
		notFound,
	}

	async function notFound () {
		return error(new Error('Not found.'), 404)
	}
}
