import { errorCatcher } from '.'
import { ok, error } from '../helpers/http-response'
import errors from '../helpers/errors'

export default function makeUsuarioController({ usuarioUseCase }) {
  return {
    login,
    verifyToken,
  }

  async function login (req) {
    return await errorCatcher(usuarioUseCase.login, req.body)
  }

  async function verifyToken (req, res, next) {
    const token = req.headers['x-access-token']
    try {
      req.jwt = await usuarioUseCase.verifyToken(token)
      next()
    } catch (err) {
      const isKnownError = Object.values(errors).some(ErrorClass => err instanceof ErrorClass)
      if (isKnownError) {
        const e = error(err, 401)

        if (e.headers) res.set(e.headers)
        res
          .status(e.statusCode)
          .json(e.body)

        console.log(`response ${e.statusCode}:\n${JSON.stringify(e.body, null, 4)}\n`)
      } else {
        // TODO: implement error logging
        console.error('Uknown error: ', err)
        const errorFormated = error('Ocorreu um erro desconhecido', 500)
        res
          .status(500)
          .end(errorFormated)

        console.log(`response ${500}:\n${JSON.stringify(errorFormated, null, 4)}\n`)
      }
    }
  }
}

