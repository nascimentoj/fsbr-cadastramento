import { errorCatcher } from '.'

export default function makeCadastroController({ cadastroUseCase }) {
  return {
    get,
    find,
    findByCPF,
    post,
    put,
    remove
  }

  async function get (req) {
    return await errorCatcher(cadastroUseCase.list)
  }

  async function find (req) {
    return await errorCatcher(cadastroUseCase.find, req.query)
  }

  async function findByCPF (req) {
    return await errorCatcher(cadastroUseCase.findByCPF, req.params.cpf)
  }

  async function post (req) {
    return await errorCatcher(cadastroUseCase.add, req.body)
  }

  async function put (req) {
    return await errorCatcher(cadastroUseCase.edit, req.body)
  }

  async function remove (req) {
    return await errorCatcher(cadastroUseCase.remove, req.params.id)
  }
}

