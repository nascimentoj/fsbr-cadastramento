import { ok, error } from '../helpers/http-response'
import errors from '../helpers/errors'
import makeEstadoController from './estado'
import makeCadastroController from './cadastro'
import makeUsuarioController from './usuario'
import makeOtherController from './other'

import {
  estadoUseCase,
  cadastroUseCase,
  usuarioUseCase,
} from '../use-cases'

const estadoController = makeEstadoController({ estadoUseCase })
const cadastroController = makeCadastroController({ cadastroUseCase })
const usuarioController = makeUsuarioController({ usuarioUseCase })
const otherController = makeOtherController()

export {
  estadoController,
  cadastroController,
  usuarioController,
  otherController,
}

export async function errorCatcher (useCase, params) {
  try {
    const data = await useCase(params)
    return ok(data)
  } catch (e) {
    const isKnownError = Object.values(errors).some(ErrorClass => e instanceof ErrorClass)
    if (isKnownError) return error(e)
    else throw e
  }
}

