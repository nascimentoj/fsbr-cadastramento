import { errorCatcher } from '.'

export default function makeEstadoController({ estadoUseCase }) {
	return {
		get,
	}

  async function get (req) {
    return await errorCatcher(estadoUseCase.list)
  }
}
