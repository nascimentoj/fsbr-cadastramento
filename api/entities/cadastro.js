import message from '../helpers/message'
import { ValidationError } from '../helpers/errors'

export default function makeCadastro({
	id = null,
	nome,
	cpf,
	estado,
	cidade,
}) {
	const NOME_CHAR_LIMIT = 100
	const CIDADE_CHAR_LIMIT = 50

	if (!nome) throw new ValidationError(message.required('Nome'), 'nome')
	if (!cpf) throw new ValidationError(message.required('CPF'), 'cpf')
	if (!estado) throw new ValidationError(message.required('Estado'), 'estado')
	if (!cidade) throw new ValidationError(message.required('Cidade'), 'cidade')

	if (nome.length > NOME_CHAR_LIMIT) throw new ValidationError(message.maxChar('Nome', NOME_CHAR_LIMIT), 'nome')
	if (cidade.length > CIDADE_CHAR_LIMIT) throw new ValidationError(message.maxChar('Cidade', CIDADE_CHAR_LIMIT), 'cidade')
	if (!/^\d{11}$/.test(cpf)) throw new ValidationError('CPF inválido, deve conter 11 números', 'cpf')

	return { id, nome, cpf, estado, cidade }
}
