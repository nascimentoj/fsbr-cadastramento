import message from '../helpers/message'
import { ValidationError } from '../helpers/errors'

export default function makeUsuario({
	id = null,
	email,
	nome,
	senha,
}) {
	const EMAIL_CHAR_LIMIT = 255
	const NOME_CHAR_LIMIT = 255
	const SENHA_CHAR_LIMIT = 255

	if (!email) throw new ValidationError(message.required('e-mail'), 'email')
	if (!nome) throw new ValidationError(message.required('Nome'), 'nome')
	if (!senha) throw new ValidationError(message.required('Senha', 'senha'))

	if (email.length > EMAIL_CHAR_LIMIT) throw new ValidationError(message.maxChar('e-mail', EMAIL_CHAR_LIMIT), 'email')
	//if (nome.length > NOME_CHAR_LIMIT) throw new ValidationError(message.maxChar('Nome', NOME_CHAR_LIMIT), 'nome')
	if (senha.length > SENHA_CHAR_LIMIT) throw new ValidationError(message.maxChar('Senha', NOME_CHAR_LIMIT), 'senha')

	return { id, nome, email, senha }
}
