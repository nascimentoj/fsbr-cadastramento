const TABLE_NAME = 'usuario'
/*
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | bigint(20)   | NO   | PRI | <null>  | auto_increment |
| email | varchar(255) | YES  | UNI | <null>  |                |
| nome  | varchar(255) | YES  |     | <null>  |                |
| senha | varchar(255) | YES  |     | <null>  |                |
+-------+--------------+------+-----+---------+----------------+
*/

export default function makeCadastroDb ({ makeDb, releaseConnection }) {
  return {
    list,
    findById,
    findByEmail,
    login,
		add,
		edit,
		remove
  }

  async function list () {
    const db = await makeDb()
    const [ rows ] = await db.query(`select * from ${TABLE_NAME}`)
    releaseConnection(db)
    return rows
  }

  async function findById (id) {
    const db = await makeDb()
    const [ rows ] = await db.execute(`select * from ${TABLE_NAME} where id = ?`, [ id ])
    releaseConnection(db)
    return rows.length ? rows [0] : null
  }

  async function findByEmail (email) {
    const db = await makeDb()
    const [ rows ] = await db.execute(`select * from ${TABLE_NAME} where email = ?`, [ email ])
    releaseConnection(db)
    return rows.length ? rows [0] : null
  }

  async function login ({ email, senha }) {
    const db = await makeDb()
    const [ rows ] = await db.execute(
      `select * from ${TABLE_NAME} where email = ? and senha = ?`,
      [ `%${email}%`, `%${senha}%` ]
    )
    releaseConnection(db)
    return rows.length ? rows[0] : null
  }

  async function add ({ email, nome, senha }) {
    const db = await makeDb()
    const [ { insertId: id } ] = await db.execute(
      `insert into ${TABLE_NAME} (email, nome, senha) values (?, ?, ?)`, [
      email, nome, senha
    ])
    releaseConnection(db)
    return id
  }

  async function edit ({ id, email, nome, senha }) {
    const db = await makeDb()
    const [ { changedRows } ] = await db.execute(
      `update ${TABLE_NAME} set email = ?, nome = ?, senha = ?
      where id = ?`, [
      email, nome, senha, id
    ])
    releaseConnection(db)
    return changedRows > 0
  }

  async function remove (id) {
    const db = await makeDb()
    const [ { affectedRows } ] = await db.execute(
      `delete from ${TABLE_NAME} where id = ?`, [ id ]
    )
    releaseConnection(db)
    return affectedRows > 0
  }
}
