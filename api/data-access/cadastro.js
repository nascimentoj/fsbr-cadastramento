const TABLE_NAME = 'cadastramento'
/*
+--------+---------------------+------+-----+---------+----------------+
| Field  | Type                | Null | Key | Default | Extra          |
+--------+---------------------+------+-----+---------+----------------+
| nome   | varchar(100)        | NO   |     | <null>  |                |
| cpf    | varchar(22)         | NO   |     | <null>  |                |
| cidade | varchar(50)         | YES  |     | <null>  |                |
| estado | varchar(100)        | YES  |     | <null>  |                |
| id     | bigint(20) unsigned | NO   | PRI | <null>  | auto_increment |
+--------+---------------------+------+-----+---------+----------------+
*/

export default function makeCadastroDb ({ makeDb, releaseConnection }) {
  return {
    list,
		find,
    findByCPF,
		add,
		edit,
		remove
  }

  async function list () {
    const db = await makeDb()
    const [ rows ] = await db.query(`select * from ${TABLE_NAME}`)
    releaseConnection(db)
    return rows
  }

  async function findByCPF (cpf) {
    const db = await makeDb()
    const [ rows ] = await db.execute(`select * from ${TABLE_NAME} where cpf = ?`, [ cpf ])
    releaseConnection(db)
    return rows.length ? rows[0] : null
  }

  async function find ({ nome, cpf }) {
    const db = await makeDb()
    const [ rows ] = await db.execute(
      `select * from ${TABLE_NAME} where nome like ? or cpf like ?`,
      [ nome && `%${nome}%`, cpf && `%${cpf}%` ]
    )
    releaseConnection(db)
    return rows
  }

  async function add ({ nome, cpf, cidade, estado }) {
    const db = await makeDb()
    const [ { insertId: id } ] = await db.execute(
      `insert into ${TABLE_NAME} (nome, cpf, cidade, estado) values (?, ?, ?, ?)`, [
      nome, cpf, cidade, estado
    ])
    releaseConnection(db)
    return id
  }

  async function edit ({ id, nome, cpf, cidade, estado }) {
    const db = await makeDb()
    const [ { changedRows } ] = await db.execute(
      `update ${TABLE_NAME} set nome = ?, cpf = ?, cidade = ?, estado = ?
      where id = ?`, [
        nome, cpf, cidade, estado, id
    ])
    releaseConnection(db)
    return changedRows > 0
  }

  async function remove (id) {
    const db = await makeDb()
    const [ { affectedRows } ] = await db.execute(
      `delete from ${TABLE_NAME} where id = ?`, [ id ]
    )
    releaseConnection(db)
    return affectedRows > 0
  }
}
