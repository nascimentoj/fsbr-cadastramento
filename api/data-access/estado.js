const TABLE_NAME = 'estados'
/*
+--------+---------------------+------+-----+---------+----------------+
| Field  | Type                | Null | Key | Default | Extra          |
+--------+---------------------+------+-----+---------+----------------+
| id     | bigint(20) unsigned | NO   | PRI | <null>  | auto_increment |
| estado | varchar(100)        | NO   |     | <null>  |                |
+--------+---------------------+------+-----+---------+----------------+
*/

export default function makeEstadoDb ({ makeDb, releaseConnection }) {
  return {
    list
  }

  async function list () {
    const db = await makeDb()
    try {
      const res = await db.query(`select * from ${TABLE_NAME}`)
      releaseConnection(db)
      return res[0]
    } catch (err) {
      throw err
    }
  }
}
