import mysql from 'mysql2'
import dotenv from 'dotenv'
import makeCadastroDb from './cadastro'
import makeEstadoDb from './estado'
import makeUsuarioDb from './usuario'

dotenv.config()

const {
  FRSB_CAD_DB_URL: URL,
  FRSB_CAD_DB_NAME: NAME,
  FRSB_CAD_DB_USER: USER,
  FRSB_CAD_DB_PASSWORD: PASSWORD,
} = process.env

const pool = mysql.createPool({
  host: URL,
  database: NAME,
  user: USER,
  password: PASSWORD
})

async function makeDb() {
  // NOTE: don't forget to pool.release() after using.
  try {
    // TODO: there's something wrong here
    // after several requests this .getConnection
    // get stuck! I think is something about
    // releasing connections although i'm releasing
    // them in the database interfaces... it could
    // be some problem with the api too.
    return await pool.promise().getConnection()
  } catch (err) {
    throw new Error('Error connecting to database', err)
  }
}

const releaseConnection = conn => pool.releaseConnection(conn)

const cadastroDb = makeCadastroDb({ makeDb, releaseConnection })
const estadoDb = makeEstadoDb({ makeDb, releaseConnection })
const usuarioDb = makeUsuarioDb({ makeDb, releaseConnection })

export {
	cadastroDb,
	estadoDb,
  usuarioDb,
}

