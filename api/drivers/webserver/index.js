import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import {
  usuarioController,
  estadoController,
  cadastroController,
  otherController,
} from '../../controllers/index'
import makeCallback from '../../helpers/express-callback'

dotenv.config()

const { 
  FRSB_CAD_API_PORT: PORT,
} = process.env

const app = express()
app.use(cors())
app.use(bodyParser.json())

app.post('/api/login', makeCallback(usuarioController.login))
app.use(usuarioController.verifyToken)
app.get('/api/estado', makeCallback(estadoController.get))
app.get('/api/cadastro', makeCallback(cadastroController.get))
app.get('/api/cadastro/consulta', makeCallback(cadastroController.find))
app.get('/api/cadastro/:cpf', makeCallback(cadastroController.findByCPF))
app.post('/api/cadastro', makeCallback(cadastroController.post))
app.put('/api/cadastro', makeCallback(cadastroController.put))
app.delete('/api/cadastro/:id', makeCallback(cadastroController.remove))
app.use(makeCallback(otherController.notFound))

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`)
})

export default app

