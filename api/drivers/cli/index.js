import { createInterface } from 'readline'
import { usuarioUseCase } from '../../use-cases'
import errors from '../../helpers/errors'

const rl = createInterface({
	input: process.stdin,
	output: process.stdout
})

let option

class GoMainMenuError extends Error {}

do {
  option = await ask(`Command Line Interface meant to be a safe way to manage users without login


  What do you want to do:
  (1) list
  (2) create
  (3) update
  (4) delete
  (5) exit
  
  option: `)
  console.log('\n\n')

  try {
    switch (Number(option)) {
      case 1:
        await list()
        break
      case 2:
        await create()
        break
      case 3:
        await update()
        break
      case 4:
        await remove()
        break
      case 5:
        process.exit()
        break
      default:
        console.log('Invalid option!\n\n')
        break
    }
  } catch(e) {
    if (e instanceof GoMainMenuError) {
      option = 'none'
    } else {
      const isKnownError = Object.values(errors).some(ErrorClass => e instanceof ErrorClass)
      if (isKnownError) console.log(`${e.message}\n`)
      else throw e
    }
  }

  if (option !== 'none') await ask('Press return to continue...')
} while (5 !== option)

async function askUserInfo() {
  const nome = await ask('nome: ')
  const email = await ask('email: ')
  const senha = await ask('senha: ')
  return { nome, email, senha }
}

async function create () {
  console.log('Create user:\n\n')
  const user = await askUserInfo()
  const message = await usuarioUseCase.add(user)
  console.info(message)
}

async function askId () {
  const id = await ask('(b) to go to main menu\n\nid: ')
  if (/\d+/.test(id)) return Number(id)
  else if (id !== 'b') return await askId()
  else throw new GoMainMenuError()
}

async function update () {
  let id, user
  do {
    console.log('Update user:\n\n')
    id = await askId()

    user = await usuarioUseCase.findById(id)

    if (user) {
      console.log(`\nUser found:\n${JSON.stringify(user, null, 2)}\n\nEnter with the new data:`)

      user = await askUserInfo()
      const message = await usuarioUseCase.edit({ id, ...user })
      console.info(message)
    } else console.log('User not found!\n\n')
  } while (id != 'b' || typeof user != 'object')
}

async function remove() {
  console.log('Delete user:\n\n')
  const id = await askId()

  const message = await usuarioUseCase.remove(id)
  console.info(message)
}

async function list() {
  console.log('List user:\n\n')
  const users = await usuarioUseCase.list()
  console.info(`${JSON.stringify(users, null, 2)}\n\n`)
}

function ask(questionText) {
  return new Promise((resolve, reject) => {
    rl.question(questionText, (input) => resolve(input))
  })
}

