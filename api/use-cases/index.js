import dotenv from 'dotenv'
import makeCadastroUseCase from './cadastro'
import makeEstadoUseCase from './estado'
import makeUsuarioUseCase from './usuario'

import {
  cadastroDb,
  estadoDb,
  usuarioDb,
} from '../data-access'

dotenv.config()

const {
  FRSB_CAD_API_SECRET: SECRET,
} = process.env

const estadoUseCase = makeEstadoUseCase({ estadoDb })
const cadastroUseCase = makeCadastroUseCase({ cadastroDb })
const usuarioUseCase = makeUsuarioUseCase({ usuarioDb, SECRET })

export {
  estadoUseCase,
	cadastroUseCase,
	usuarioUseCase,
}

