import { makeCadastro } from '../entities'
import { ValidationError, DbError } from '../helpers/errors'
import { makeSuccessMessage } from '../helpers/message'

export default function makeCadastroUseCase ({ cadastroDb }) {
	return {
		list,
		find,
    findByCPF,
		add,
		edit,
		remove
	}

  async function list() {
    // no business rules implement
    return await cadastroDb.list()
  }

  async function find(search) {
    const res = await cadastroDb.find(search)
    if (res.length) return res
    else throw new ValidationError('Dados não localizado, tente novamente')
  }

  async function findByCPF(cpf) {
    const res = await cadastroDb.findByCPF(cpf)
    if (res) return res
    else throw new ValidationError('Dados não localizado, tente novamente')
  }

  async function add(data) {
		const cadastro = makeCadastro(data)

    const insertErrorMsg = 'Houve algum problema na inclusão, tentar novamente'

    try {
      cadastro.id = await cadastroDb.add(cadastro)
    } catch (e) {
      throw new DbError(insertErrorMsg)
    }

    if (cadastro.id > 0) return makeSuccessMessage('Cadastro realizado com sucesso')
    else throw new DbError(insertErrorMsg)
  }

  async function edit(data) {
    checkId(data.id)
		const cadastro = makeCadastro(data)

    let isChanged
    try {
      isChanged = await cadastroDb.edit(cadastro)
    } catch (e) {
      throw new DbError('Houve algum problema na alteração, tentar novamente')
    }

    if (isChanged) return makeSuccessMessage('Cadastro atualizado com sucesso')
    else throw new ValidationError('Nenhum dado foi alterado no registro')
  }

  async function remove(id) {
    checkId(id)
    let isDeleted

    try {
      isDeleted = await cadastroDb.remove(id)
    } catch (e) {
      throw new DbError('Houve algum problema na exclusão, tentar novamente')
    }

    if (isDeleted) return makeSuccessMessage('Cadastro excluído com sucesso')
    else throw new ValidationError('Registro não encontrado')
  }

  function checkId (id) {
    if (!id) throw new ValidationError('Dados inconsistentes, busque o cadastro novamente', 'id')
  }
}
