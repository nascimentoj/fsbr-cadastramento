import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import { makeUsuario } from '../entities'
import { ValidationError, DbError } from '../helpers/errors'
import { makeSuccessMessage } from '../helpers/message'

export default function makeCadastroUseCase ({ usuarioDb, SECRET }) {
	return {
		list,
    findById,
    login,
		add,
		edit,
		remove,
    verifyToken,
	}

  async function list() {
    // no business rules implement
    return await usuarioDb.list()
  }

  async function findById(id) {
    // no business rules implement
    return await usuarioDb.findById(id)
  }

  async function login({ email, senha }) {
    const usuario = await usuarioDb.findByEmail(email)

    if (!usuario) throw new ValidationError('E-mail não cadastrado, tente novamente', 'email')

    const MINUTES_EXPIRE = 5

    const validPassword = await bcrypt.compare(senha, usuario.senha)

    if (validPassword) {
      const { id, nome, email } = usuario
      const token = jwt.sign({ id, nome, email }, SECRET, {
        expiresIn: MINUTES_EXPIRE * 60
      })
      return { token }
    } else throw new ValidationError('Senha incorreta, tente novamente', 'senha')
  }

  async function verifyToken(token) {
    if (!token) throw new ValidationError('Token não informado', 'token')

    try {
      return await jwt.verify(token, SECRET)
    } catch (err) {
      throw new ValidationError('Falha ao autenticar token', 'token')
    }
  }

  async function add(data) {
    const salt = await bcrypt.genSalt(10)
    data.senha = await bcrypt.hash(data.senha, salt)

		const usuario = makeUsuario(data)

    const insertErrorMsg = 'Houve algum problema na inclusão, tentar novamente'
    try {
      usuario.id = await usuarioDb.add(usuario)
    } catch (e) {
      throw new DbError(insertErrorMsg)
    }

    if (usuario.id > 0) return makeSuccessMessage('Cadastro realizado com sucesso')
    else throw new DbError(insertErrorMsg)
  }

  async function edit(data) {
    checkId(data.id)
    const salt = await bcrypt.genSalt(10)
    data.senha = await bcrypt.hash(data.senha, salt)

		const usuario = makeUsuario(data)

    let isChanged
    try {
      isChanged = await usuarioDb.edit(usuario)
    } catch (e) {
      throw new DbError('Houve algum problema na alteração, tentar novamente')
    }

    if (isChanged) return makeSuccessMessage('Cadastro atualizado com sucesso')
    else throw new ValidationError('Nenhum dado foi alterado no registro')
  }

  async function remove(id) {
    checkId(id)
    let isDeleted
    try {
      isDeleted = await usuarioDb.remove(id)
    } catch (e) {
      throw new DbError('Houve algum problema na exclusão, tentar novamente')
    }
    
    if (isDeleted) return makeSuccessMessage('Cadastro excluído com sucesso')
    else throw new ValidationError('Registro não encontrado')
  }

  function checkId (id) {
    if (!id) throw new ValidationError('Dados inconsistentes, busque o usuario novamente', 'id')
  }
}
