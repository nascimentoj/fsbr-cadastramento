export const required = fieldName => `O campo ${fieldName} é obrigatório`
export const maxChar = (fieldName, limit) => `${fieldName} deve conter no máximo ${limit} caracteres`

export const makeSuccessMessage = message => ({
  ok: true, message
})

export default { required, maxChar  }
