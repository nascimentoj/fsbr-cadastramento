// this is a module to register custom errors

export class ValidationError extends Error {
  constructor(message, field) {
    super(message)
    this.message = message
    this.name = 'ValidationError'
    this.field = field
  }
}

export class DbError extends Error {
  constructor(message, field) {
    super(message)
    this.name = 'DbError'
  }
}

export default {
  ValidationError,
  DbError
}
