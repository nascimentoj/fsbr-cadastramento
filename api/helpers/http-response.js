// get
const jsonHeader = {
	'Content-Type': 'application/json'
}

// 201 for post
// lastm in update
// delete: statusCode: deleted.deletedCount === 0 ? 404 : 200,
export function ok (body, statusCode = 200, lastModified) {
  const headers = lastModified ? { 
    ...jsonHeader,
    'Last-Modified': lastModified.toUTCString()
  } : jsonHeader

  return {
    headers: jsonHeader,
    statusCode,
    body
  }
}

export function error({ name: type, message, ...e }, statusCode = 400) {
  return {
    headers: jsonHeader,
    statusCode: e.name === 'RangeError' ? 404 : statusCode,
    body: {
      error: {
        type, message, ...e
      }
    }
  }
}

