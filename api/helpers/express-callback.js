import { error } from './http-response'
export default function makeExpressCallback (controller) {
  return async (req, res) => {
    const httpRequest = {
      body: req.body,
      query: req.query,
      params: req.params,
      ip: req.ip,
      method: req.method,
      path: req.path,
      headers: {
        'Content-Type': req.get('Content-Type'),
        Referer: req.get('referer'),
        'User-Agent': req.get('User-Agent')
      }
    }

    console.log(`request ${req.method} ${req.url} to controller action ${controller.name}.
      query: ${JSON.stringify(req.query)}
      params: ${JSON.stringify(req.params)}
      body: ${JSON.stringify(req.body, null, 4)}`)

    try {
      const httpResponse = await controller(httpRequest)
      if (httpResponse.headers) {
        res.set(httpResponse.headers)
      }
      res.type('json')
      res
        .status(httpResponse.statusCode)
        .send(httpResponse.body)
      
      console.log(`response ${httpResponse.statusCode}:\n${JSON.stringify(httpResponse.body, null, 4)}\n`)
    } catch (e) {
      // TODO: implement error logging
      console.error('Uknown error: ', e)
      const err = error({ ...e, message: 'Ocorreu um erro desconhecido' }, 500)
      res
        .status(500)
        .send(err.body)

      console.log(`response ${500}:\n${JSON.stringify(err.body, null, 4)}\n`)
    }
  }
}
